An extremely simple, entirely client-side bingo card. Seeding is done via CRC32 of a user-provided name, and clicked cells are remembered via localStorage.

### Quick Start

You will need to have a semi recent node.js version installed (I have not tested old versions).  The first time,
to get set up, do these steps:

1. Clone this repo
2. Run `npm install`

To start developer mode (which auto-updates live when you make changes and save):

1. Run `npm start`
2. Click the link after "Server Running, usually http://localhost:1234

If you edit and save a file in `src/` you will see the changes live in your browser.


### Production Build

To make a version of the page suitable for hosting as a static website:

1. Run `npm run build`
2. Upload the contents of the `dist/` folder to somewhere that does static site hosting

Easy places to host a static site are things like Cloudflare Pages, GitLab Pages, Netlify, etc

# License

Released under the terms of the MIT license.  See full license in [LICENSE.md](LICENSE.md)
