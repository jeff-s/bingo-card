import crc32 from 'crc-32';
import {XORShift} from 'random-seedable';

const CELL_VALUES = [
  'Event 1',
  'Event 2',
  'Event 3',
  'Event 4',
  'Event 5',
  'Event 6',
  'Event 7',
  'Event 8',
  'Event 9',
  'Event 10',
  'Event 11',
  'Event 12',
  'Event 13',
  'Event 14',
  'Event 15',
  'Event 16',
  'Event 17',
  'Event 18',
  'Event 19',
  'Event 20',
  'Event 21',
  'Event 22',
  'Event 23',
  'Event 24',
  'Event 25',
  'Event 26',
  'Event 27',
  'Event 28',
  'Event 29',
  'Event 30',
  //...
  // You can have any number of events >=24 that you want, but changing the quantity or order of events in this list
  // will change which ones end up showing up on any given person's bingo card.
];


/*
Small helpers for working with browser localStorage API, and tracking the state
This is a simple global state because it's a simple page.
*/

function readState() {
  let stateStr = localStorage.getItem('state');
  if (stateStr) {
    return JSON.parse(stateStr);
  }
}

let state = readState() || {
  username: null,
  checkedValues: {}
};

function writeState() {
  localStorage.setItem('state', JSON.stringify(state));
}


/*
Other constants / DOM mapping shenanigans
*/

const CHECKED_CLASS = 'checked';

const CELL_IDS = [
  'b0', 'i0', 'n0', 'g0', 'o0',
  'b1', 'i1', 'n1', 'g1', 'o1',
  'b2', 'i2', /* */ 'g2', 'o2',
  'b3', 'i3', 'n3', 'g3', 'o3',
  'b4', 'i4', 'n4', 'g4', 'o4'
];

const EMPTY_VALUES = CELL_IDS.map(_ => '');


/*
Helper for setupTypingEvents() which updates the card DOM to match what the values should be
*/

function fillCard(username) {
  // start with just empty values, so there is no bingo card until the usertypes their username
  let shuffledValues = EMPTY_VALUES;
  // deterministically shuffle list of values if there is a username
  // CRC32 is good enough for our purposes here
  const crcKey = String(username || '').toLowerCase().replace(/[^A-Za-z0-9_-]/g, '');
  if (crcKey) {
    const crc = crc32.str(crcKey);
    const rng = new XORShift(crc, 13, 17, 5);
    shuffledValues = rng.shuffle(CELL_VALUES, false);
  }
  //fill out the table
  for (let i = 0; i < CELL_IDS.length; i++) {
    let id = CELL_IDS[i];
    let elt = document.getElementById(id);
    if (elt) {
      let value = shuffledValues[i];
      elt.innerText = value;
      elt.classList.toggle(CHECKED_CLASS, !!state.checkedValues[value]);
    }
  }
}

/*
When user types in their username, reshuffle the card based on what they typed
Do this with a slight debounce (wait until they are done typing to update the card)
*/
function setupTypingEvents() {
  let usernameInput = document.getElementById('crckey');
  let nameTypedDebounceTimeout = null;

  function onNameTyped(event) {
    if (nameTypedDebounceTimeout) {
      clearTimeout(nameTypedDebounceTimeout);
    }
    nameTypedDebounceTimeout = setTimeout(_ => {
      let username = usernameInput.value;
      usernameInput.classList.toggle('empty', !username);
      state.username = username;
      fillCard(username);
      writeState();
      nameTypedDebounceTimeout = null;
    }, 600);
  }

  usernameInput.addEventListener('input', onNameTyped);
  usernameInput.addEventListener('change', onNameTyped);
  usernameInput.value = state.username;
  fillCard(state.username);
  usernameInput.classList.toggle('empty', !state.username);
  if (!state.username) {
    usernameInput.focus();
  }
}

/*
When user clicks on a bingo cell, toggle it between on and off, and remember
its state in localStorage
*/
function setupClickEvents() {
  document.getElementById('bingo').addEventListener('click', event => {
    let cell = event && event.target && event.target.closest('.cell');
    if (!cell) return;
    let value = cell.innerText;
    if (value && cell.id !== 'free') {
      let newState = !state.checkedValues[value];
      if (newState) {
        state.checkedValues[value] = newState;
        cell.classList.add(CHECKED_CLASS);
      } else {
        delete state.checkedValues[value];
        cell.classList.remove(CHECKED_CLASS);
      }
      writeState();
    }
  });
}


/*
Time to get this party started!
*/

setupClickEvents();
setupTypingEvents();
